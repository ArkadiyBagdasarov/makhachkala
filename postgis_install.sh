wget http://download.osgeo.org/gdal/2.0.2/gdal-2.0.2.tar.gz
tar xzf ./gdal-2.0.2.tar.gz
cd gdal-2.0.2/
./configure
make
sudo make install

cd ../

wget http://download.osgeo.org/geos/geos-3.5.0.tar.bz2
tar xjf geos-3.5.0.tar.bz2
cd geos-3.5.0
./configure
make
sudo make install

cd ../

wget http://download.osgeo.org/proj/proj-4.9.2.tar.gz
wget http://download.osgeo.org/proj/proj-datumgrid-1.5.tar.gz
tar xzf proj-4.9.2.tar.gz
cd proj-4.9.2/nad
tar xzf ../../proj-datumgrid-1.5.tar.gz
cd ..
./configure
make
sudo make install

cd ../

wget http://download.osgeo.org/postgis/source/postgis-2.1.8.tar.gz
tar xzf postgis-2.1.8.tar.gz
cd postgis-2.1.8
./configure
make
sudo make install
