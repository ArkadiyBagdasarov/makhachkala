API_CHECK_CSRF = False
DEBUG = False
ALLOWED_HOSTS = ['95.213.235.239']

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'mobile',
        'USER': 'mobile',
        'PASSWORD': 'django123',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}