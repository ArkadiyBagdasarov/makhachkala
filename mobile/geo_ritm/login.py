from django.conf import settings
from geo_ritm.base import request_to_georitm


def login():
    data = {
        'login': settings.GEORITM_LOGIN,
        'password': settings.GEORITM_PASSWORD
    }
    resp = request_to_georitm('users/login/', data)
    return resp