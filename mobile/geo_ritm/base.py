import requests
from django.conf import settings


def request_to_georitm(url, data):
    resp = requests.post(
        settings.GEORITM_URL_API + url,
        json=data,
        headers={'Authorization': 'Basic bWFoYWNoa2FsYTp3Ym5oZnZqeQ=='}
    )
    return resp.json()
