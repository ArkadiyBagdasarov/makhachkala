import datetime

from geo_ritm.base import request_to_georitm


class GeoRitmApiCore:
    def __init__(self):
        self.datetime_format = '%Y-%m-%d %H:%M:%S'

    def all_bus(self):
        data = {"id": 3281}
        resp = request_to_georitm('objs-groups/group/', data)
        return resp

    def all_active_bus(self, buses):
        return [bus for bus in buses if bus['isGsmOnline'] and bus['isGpsOnline']]

    def get_all_bus(self):
        self.buses = self.all_bus()['objects']
        return self.buses

    def get_active_bus(self):
        self.get_all_bus()
        return self.all_active_bus(self.buses)

    def get_bus_info(self, bus_ids):
        if not isinstance(bus_ids, list):
            bus_ids = [bus_ids]

        data = {"objectId": bus_ids}

        resp = request_to_georitm('objects/obj/', data)
        return resp

    def get_bus_in_zones(self):
        now = datetime.datetime.now()
        now_str = now.strftime('%Y-%m-%d %H:%M:%S')
        hour_ago = (now - datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
        data = {
            "zoneId": [3396], "objectId": [i['id'] for i in self.get_all_bus()],
            "from": hour_ago,
            "to": now_str,
            "tzId": "Europe/Moscow",
            "scode": "YDX", "sort": "id"
        }
        resp = request_to_georitm('reports/ZONEVIZ/', data)
        warning_buses = [i['name'] for i in resp['footer'][0]['visits'] if i['distOut']]
        return warning_buses
