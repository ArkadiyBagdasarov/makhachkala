import numpy as np
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from route.models import RoutePath


class NearestCore:
    def __init__(self, **kwargs):
        self.point_start = kwargs['point_start']
        self.point_stop = kwargs['point_stop']

        self.routes_path = list(
            RoutePath.objects.annotate(
                distance_from_start=Distance('path_from', Point(self.point_start, srid=4326)),
                distance_from_stop=Distance('path_from', Point(self.point_stop, srid=4326)),
                distance_to_start=Distance('path_to', Point(self.point_start, srid=4326)),
                distance_to_stop=Distance('path_to', Point(self.point_stop, srid=4326)),
            )
            .values('id', 'distance_from_start',
                    'distance_from_stop',
                    'distance_to_start',
                    'distance_to_stop',
                    'path_from', 'route',
                    'path_to', 'route__name',
                    'stop_to', 'stop_from',
                    'route'
                    )
        )
        self.course = self._get_course()

    def get_neirest(self):
        bus_stop_map = self._get_stop()
        return bus_stop_map

    def _get_stop(self):
        bus_stop = []
        for r in self.routes_path:
            current_point_start = self._get_current_point(r['path_' + self.course], self.point_start)
            current_point_stop = self._get_current_point(r['path_' + self.course], self.point_stop)

            start_bus_stop = self._get_index_stop(r['stop_' + self.course], current_point_start)
            stop_bus_stop = self._get_index_stop(r['stop_' + self.course], current_point_stop)

            start_bus_stop['distance_' + self.course + '_start'] = r['distance_' + self.course + '_start'].m
            stop_bus_stop['distance_' + self.course + '_stop'] = r['distance_' + self.course + '_stop'].m

            start_bus_stop['route_id'] = r['route']
            stop_bus_stop['route_id'] = r['route']
            bus_stop.append([start_bus_stop, stop_bus_stop])
        return bus_stop

    def _get_index_stop(self, stop_points, index_point):
        indexes = np.asarray(
            [i['current_index'] for i in stop_points]
        )
        point = self.find_nearest_point(indexes, index_point)
        return stop_points[point]

    def _get_course(self):
        r = self.routes_path[0]
        current_point_start = self._get_current_point(r['path_to'], self.point_start)
        current_point_stop = self._get_current_point(r['path_to'], self.point_stop)
        return 'to' if current_point_stop > current_point_start else 'from'

    def _get_current_point(self, points, point):
        path = np.asarray(points)
        dist = np.sum((path - point) ** 2, axis=1)
        current_point = np.argmin(dist)
        return current_point

    def find_nearest_point(self, array, value):
        idx = (np.abs(array - value)).argmin()
        return idx
