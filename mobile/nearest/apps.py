from django.apps import AppConfig


class NearestConfig(AppConfig):
    name = 'nearest'
