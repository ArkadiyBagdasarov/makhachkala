import logging

from django.db import connections
from django.template import Template, Context
from django.conf import settings
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
logger = logging.getLogger('sql_debug_log')


class UpdateLastActivityMiddleware(MiddlewareMixin):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated():
            return

        account = getattr(request.user, 'account', None)

        if account is not None:
            account.last_activity_time = timezone.now()
            account.save(update_fields=['last_activity_time'])


class SQLLogMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        if settings.DEBUG:
            for connection_name in connections:
                logger.info('[SQLLogMiddleware] conn_name = %s', connection_name)
                connection = connections[connection_name]
                logger.info('[SQLLogMiddleware] connection.queries = %s', connection.queries)
                if connection.queries:
                    logger.info('[SQLLogMiddleware] in')
                    time = sum([float(q['time']) for q in connection.queries])
                    header_t = Template("{{name}}: {{count}} quer{{count|pluralize:\"y,ies\"}} in {{time}} seconds")
                    logger.info(header_t.render(Context({
                        'name': connection_name,
                        'sqllog': connection.queries,
                        'count': len(connection.queries),
                        'time': time
                    })))
                    t = Template("{% for sql in sqllog %}[{{forloop.counter}}] {{sql.time}}s: {{sql.sql|safe}}{% if "
                                 "not forloop.last %}\n\n{% endif %}{% endfor %}")
                    logger.info(t.render(Context({'sqllog': connection.queries})))

        return response
