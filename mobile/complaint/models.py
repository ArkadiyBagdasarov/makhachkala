import datetime

from bus.models import Bus
from django.contrib.auth.models import User
from django.db import models
from route.models import Route


class Complaint(models.Model):
    user = models.ForeignKey(User, verbose_name='создатель жалобы', null=True)
    route = models.ForeignKey(Route, verbose_name='маршрут')
    bus = models.CharField(verbose_name='автобус', max_length=20)
    text = models.TextField()
    time_complaint = models.DateTimeField(default=datetime.datetime.now)

    def __str__(self):
        return 'route: {}, bus: {}'.format(self.route_id, self.bus)


class Reason(models.Model):
    complaint = models.ForeignKey(Complaint, verbose_name='жалоба')
    smoke = models.BooleanField(default=False, verbose_name='курение в салоне')
    deviation = models.BooleanField(default=False, verbose_name='отклонение от курса')
    speaking = models.BooleanField(default=False, verbose_name='разговор по телефону')
    stop_in_wrong_place = models.BooleanField(default=False, verbose_name='остановка в неположенном месте')

    def __str__(self):
        return str(self.complaint_id)
