from complaint import models
from django.contrib import admin

admin.site.register(models.Complaint)
admin.site.register(models.Reason)

