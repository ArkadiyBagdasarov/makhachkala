from urllib.parse import parse_qs, urlparse

import mechanicalsoup
import numpy as np
import time

from django.db import transaction

from wialon.models import WialonAccessToken


def get_bus(buses, bus_ritm_id):
    try:
        bus = (
            item
            for item in buses
            if item['bus_ritm_id'] == bus_ritm_id
        ).__next__()
    except StopIteration:
        return None
    else:
        return bus


def get_current_point(line, lon, lat):
    nodes = np.asarray(line)
    dist = np.sum((nodes - [lon, lat]) ** 2, axis=1)
    current_point = np.argmin(dist)
    return current_point


class GetAccessTokenWialon:
    def __init__(self, login, password):
        self.browser = mechanicalsoup.StatefulBrowser(soup_config={'features': 'lxml'})
        self.login = login
        self.password = password

    def start(self):
        self.browser.open("http://hosting.wialon.com/login.html")
        self.browser.select_form('#auth-form')
        self.browser["login"] = self.login
        self.browser["passw"] = self.password

        time.sleep(1)

        resp = self.browser.submit_selected()
        token = self.get_token_from_url(resp.url)

        self.write(token)

    def get_token_from_url(self, url):
        query_params = parse_qs(urlparse(url).query)
        try:
            return query_params['access_token'][0]
        except (KeyError, IndexError):
            return None

    def write(self, token):
        if token:
            with transaction.atomic():
                WialonAccessToken.objects.all().delete()
                WialonAccessToken.objects.create(token=token)
