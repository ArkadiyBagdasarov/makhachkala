from django.apps import AppConfig


class PeriodicTasksConfig(AppConfig):
    name = 'periodic_tasks'
