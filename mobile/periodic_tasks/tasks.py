import logging
from celery import shared_task
from django.contrib.gis.geos import Point
from django.db import transaction
from django.utils import timezone
from notification.tasks import sms_notification
from periodic_tasks.utils import get_bus, get_current_point, GetAccessTokenWialon
from route.models import RoutePath

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)


@shared_task(name='get_bus_coordinats')
def get_bus_coordinats():
    from geo_ritm.bus_info import GeoRitmApiCore
    from bus.models import Bus, BusInfo

    logging.info('[GET COORDINATS] START')

    geo_ritm_obj = GeoRitmApiCore()
    active_buses = geo_ritm_obj.get_active_bus()

    logging.info('[GET COORDINATS] find buses: {}'.format(len(active_buses)))

    all_bus = list(
        Bus.objects.values(
            'id', 'bus_ritm_id', 'bus_route_id'
        )
    )
    route_path = list(
        RoutePath.objects
            .values('route', 'points_to', 'points_from')
    )

    route_path = {
        i['route']: {
            'points_to': i['points_to'],
            'points_from': i['points_from']
        }
        for i in route_path
    }

    old_bus_info = list(
        BusInfo.objects
            .values_list('bus_id', 'current_index', 'move', 'last_activity')
    )
    old_bus_info = {i[0]: [i[1], i[2], i[3]] for i in old_bus_info}

    buses = []

    for b in active_buses:
        bus = get_bus(all_bus, b['id'])
        path = route_path.get(bus['bus_route_id'])
        if not path:
            continue

        old_bus = old_bus_info.get(bus['id'])
        line = path['points_to'] + path['points_from']
        move = None
        last_index = None
        last_activity = timezone.now()

        if old_bus:
            last_index = old_bus[0]

            if old_bus[1]:
                line = path['points_' + old_bus[1]]
                move = old_bus[1]

        current_point = get_current_point(line, b['lon'], b['lat'])

        if current_point > max([len(path['points_to']), len(path['points_from'])]):
            current_point = current_point - len(path['points_to'])

        if old_bus:
            if old_bus[0]:
                if current_point < old_bus[0]:
                    if old_bus[1]:
                        move = (
                            BusInfo.COURSE_CHOICE[1][0]
                            if old_bus[1] == BusInfo.COURSE_CHOICE[0][0]
                            else BusInfo.COURSE_CHOICE[0][0]
                        )
                    else:
                        move = (
                            BusInfo.COURSE_CHOICE[1][0]
                            if current_point > len(path['points_to'])
                            else BusInfo.COURSE_CHOICE[0][0]
                        )
                    line = path['points_' + move]
                    current_point = get_current_point(line, b['lon'], b['lat'])

        if last_index:
            if current_point == last_index:
                last_activity = old_bus[2]

        bus_i = BusInfo(
            bus_id=bus['id'],
            point=Point(b['lon'], b['lat'], srid=4326),
            current_index=current_point,
            last_index=last_index,
            speed=b['speed'],
            course=b['course'],
            move=move,
            last_activity=last_activity
        )
        buses.append(bus_i)

    with transaction.atomic():
        BusInfo.objects.all().delete()
        BusInfo.objects.bulk_create(buses)

    logging.info('[GET COORDINATS] STOP')


@shared_task(name='get_bus_in_zones')
def get_bus_in_zones():
    from geo_ritm.bus_info import GeoRitmApiCore

    logging.info('[GET BUS IN ZONE] START')

    geo_ritm_obj = GeoRitmApiCore()
    warning_buses = geo_ritm_obj.get_bus_in_zones()

    if warning_buses:
        msg = 'отклонились от курса: {}'.format(', '.join(warning_buses))
        sms_notification.delay(msg)
        logging.info('[GET BUS IN ZONE] {}'.format(msg))
    else:
        logging.info('[GET BUS IN ZONE] NO BUS IN WARNING')

    logging.info('[GET BUS IN ZONE] STOP')


@shared_task(name='get_wialon_access_token')
def get_wialon_access_token():
    t = GetAccessTokenWialon('STS_API', 'os4olzl3')
    t.start()
