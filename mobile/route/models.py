from django.contrib.gis.db.models import LineStringField
from django.contrib.postgres.fields import JSONField
from django.db import models


class RoutePath(models.Model):
    path_to = LineStringField(null=True)
    path_from = LineStringField(null=True)
    points_to = JSONField(default=list)
    points_from = JSONField(default=list)
    stop_to = JSONField(default=list)
    stop_from = JSONField(default=list)

    def __str__(self):
        return self.route.name


class RouteMap(models.Model):
    map = JSONField(default=list)
    reverse_point = models.IntegerField(null=True)

    def __str__(self):
        return self.route.name


class Route(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)
    path = models.OneToOneField(RoutePath, null=True, on_delete=models.SET_NULL)
    map = models.OneToOneField(RouteMap, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name
