from django.contrib import admin
from route import models

admin.site.register(models.Route)
admin.site.register(models.RouteMap)
admin.site.register(models.RoutePath)
