import json

from django.conf import settings


from django.core.management.base import BaseCommand
from route.models import RouteMap, RoutePath


class Command(BaseCommand):
    def handle(self, *args, **options):
        routes = [
            # '125', '125a',
            # '146',
            '2'
        ]
        for route_num in routes:
            route_map = RouteMap.objects.get(route__name=route_num)
            with open('{}/{}'.format(settings.BASE_DIR, '../dict_info/yandex_route_{}_to'.format(route_num))) as f:
                data = json.loads(f.read())['data']['features']
                stop_points = [i for i in data if i['geometry']['type'] == 'Point']
                bus_stop = []
                for s in stop_points:
                    point = s['geometry']['coordinates']
                    lon = str(round(point[0], 3))
                    current_point = route_map.map.get(lon)
                    current_point = [i for i in current_point if i[2] == 'to']

                    diff = [abs(i[1][1] - point[1]) for i in current_point]
                    current_index = current_point[diff.index(min(diff))]
                    obj = {
                        'current_index': current_index[0],
                        'name': s['properties']['description'],
                        'move': current_index[2]
                    }
                    bus_stop.append(obj)
                r_path = RoutePath.objects.get(route__name=route_num)
                r_path.stop_to = bus_stop
                r_path.save()

            with open('{}/{}'.format(settings.BASE_DIR, '../dict_info/yandex_route_{}_from'.format(route_num))) as f:
                data = json.loads(f.read())['data']['features']
                stop_points = [i for i in data if i['geometry']['type'] == 'Point']
                bus_stop = []
                for s in stop_points:
                    point = s['geometry']['coordinates']
                    lon = str(round(point[0], 3))
                    current_point = route_map.map.get(lon)
                    current_point = [i for i in current_point if i[2] == 'from']

                    diff = [abs(i[1][1] - point[1]) for i in current_point]
                    current_index = current_point[diff.index(min(diff))]
                    obj = {
                        'current_index': current_index[0],
                        'name': s['properties']['description'],
                        'move': current_index[2]
                    }
                    bus_stop.append(obj)
                r_path = RoutePath.objects.get(route__name=route_num)
                r_path.stop_from = bus_stop
                r_path.save()
