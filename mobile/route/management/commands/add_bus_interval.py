from datetime import datetime, timedelta

import numpy as np
from django.conf import settings
from django.core.management.base import BaseCommand
from route.models import RoutePath


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.one()
        self.two()

    def one(self):
        routes_path = RoutePath.objects.all()

        for route_path in routes_path:
            with open('{}/{}'.format(settings.BASE_DIR, '../dict_info/route_time/{}_from_up.plt'.format(route_path.route.name))) as f:
                data = [i.split(',') for i in f.readlines()]
                points_from_plt = [[float(i[1]), float(i[0])] for i in data]

                prev_time = None
                for i in route_path.stop_to:
                    i['time'] = 0
                    p = route_path.points_to[i['current_index']]
                    nodes = np.asarray(points_from_plt)
                    dist = np.sum((nodes - p) ** 2, axis=1)
                    current_point = np.argmin(dist)

                    stamp = timedelta(seconds=0)
                    time = data[current_point][4]
                    t = datetime.strptime(time, '%H:%M:%S')

                    if prev_time:
                        stamp = t - prev_time
                        prev_time = t
                    else:
                        prev_time = t

                    if stamp.seconds > 1000:
                        i['time'] = 0
                        continue

                    i['time'] = stamp.seconds

            route_path.save(update_fields=['stop_to'])

    def two(self):
        routes_path = RoutePath.objects.all()

        for route_path in routes_path:
            with open('{}/{}'.format(settings.BASE_DIR,
                                     '../dict_info/route_time/{}_to_down.plt'.format(route_path.route.name))) as f:
                data = [i.split(',') for i in f.readlines()]
                points_to_plt = [[float(i[1]), float(i[0])] for i in data]

                prev_time = None
                for i in route_path.stop_from:
                    i['time'] = 0
                    p = route_path.points_from[i['current_index']]
                    nodes = np.asarray(points_to_plt)
                    dist = np.sum((nodes - p) ** 2, axis=1)
                    current_point = np.argmin(dist)

                    stamp = timedelta(seconds=0)
                    time = data[current_point][4]
                    t = datetime.strptime(time, '%H:%M:%S')

                    if prev_time:
                        stamp = t - prev_time
                        prev_time = t
                    else:
                        prev_time = t

                    if stamp.seconds > 1000:
                        i['time'] = 0
                        continue

                    i['time'] = stamp.seconds

            route_path.save(update_fields=['stop_from'])
