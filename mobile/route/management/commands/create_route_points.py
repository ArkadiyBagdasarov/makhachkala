import json

from django.conf import settings
from django.contrib.gis.geos import LineString
from route.models import Route


from django.core.management.base import BaseCommand
from route.models import RoutePath


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.add()

    def add(self):
        routes = [
            # '125',
            # '125a',
            # '146',
            '2',
            # '61a'
        ]
        # RoutePath.objects.all().delete()
        for route_num in routes:
            points_to = []
            with open('{}/{}'.format(settings.BASE_DIR, '../dict_info/yandex_route_{}_to'.format(route_num))) as f:
                data = json.loads(f.read())['data']['features']
                lines = [i for i in data if i['geometry']['type'] == 'LineString']
                for line_obj in lines:
                    line = line_obj['geometry']['coordinates']
                    [points_to.append([float(i[0]), float(i[1])]) for i in line]

            points_from = []
            with open('{}/{}'.format(settings.BASE_DIR, '../dict_info/yandex_route_{}_from'.format(route_num))) as f:
                data = json.loads(f.read())['data']['features']
                lines = [i for i in data if i['geometry']['type'] == 'LineString']
                for line_obj in lines:
                    line = line_obj['geometry']['coordinates']
                    [points_from.append([float(i[0]), float(i[1])]) for i in line]

            r = Route.objects.get(name=route_num)
            line_to = LineString(points_to, srid=4326)
            line_from = LineString(points_from, srid=4326)

            path = RoutePath.objects.create(
                path_to=line_to,
                path_from=line_from,
                points_to=line_to.coords,
                points_from=line_from.coords
            )
            r.path = path
            r.save(update_fields=['path'])
