from django.core.management.base import BaseCommand
from route.models import Route


class Command(BaseCommand):
    def handle(self, *args, **options):
        routes = Route.objects.filter(path__isnull=False)
        for route in routes:
            for i in route.path.stop_from:
                for r in routes:
                    for s in r.path.stop_from:
                        if s['name'] == i['name']:
                            if i.get('routes_id'):
                                i['routes_id'].append(r.id)
                            else:
                                i['routes_id'] = [r.id]
            route.path.save()

        for route in routes:
            for i in route.path.stop_to:
                for r in routes:
                    for s in r.path.stop_to:
                        if s['name'] == i['name']:
                            if i.get('routes_id'):
                                i['routes_id'].append(r.id)
                            else:
                                i['routes_id'] = [r.id]
            route.path.save()
