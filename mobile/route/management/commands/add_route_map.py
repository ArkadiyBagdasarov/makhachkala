from collections import defaultdict

from django.core.management.base import BaseCommand
from route.models import RoutePath, RouteMap


class Command(BaseCommand):
    def handle(self, *args, **options):
        route_path = RoutePath.objects.all()
        RouteMap.objects.all().delete()
        for p in route_path:
            path = defaultdict(list)
            reverse_point = len(p.path_to.coords)
            points = []

            for i, v in enumerate(p.path_to.coords):
                points.append((i, v, 'to'))

            for i, v in enumerate(p.path_from.coords):
                points.append((i, v, 'from'))

            for i, c in enumerate(points):
                path[round(c[1][0], 3)].append(c)

            route_map = RouteMap.objects.create(
                map=path,
                reverse_point=reverse_point
            )
            p.route.map = route_map
            p.route.save(update_fields=['map'])
