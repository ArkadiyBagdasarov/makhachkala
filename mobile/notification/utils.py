import logging
from notification import mainsms

logger = logging.getLogger()


def send_sms(recipients, message):
    project = 'makhachkala'
    api_key = 'edfbed86f4850'

    sms = mainsms.SMS(project, api_key)
    resp = sms.sendSMS(recipients, message, sender='makhachkala')
    logger.info(resp)
