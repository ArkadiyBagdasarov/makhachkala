import logging

from celery import shared_task
from notification.models import Phone
from notification.utils import send_sms

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)


@shared_task(name='sms_notification')
def sms_notification(msg):
    logging.info('[SMS SEND] START')
    phones = Phone.objects.values_list('number', flat=True)

    numbers = ','.join(phones)

    send_sms(numbers, msg)

    logging.info('[SMS SEND] STOP')
