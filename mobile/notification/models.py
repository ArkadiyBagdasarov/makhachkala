from django.db import models


class Phone(models.Model):
    number = models.CharField('Номер телефона', max_length=11, unique=True)

    def __str__(self):
        return self.number


class Settings(models.Model):
    start = models.TimeField(verbose_name='время начала мониторинга')
    stop = models.TimeField(verbose_name='время окончания мониторинга')
    distance = models.IntegerField(verbose_name='растояние в метрах', default=500)

    def __str__(self):
        return str(self.id)
