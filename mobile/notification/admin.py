from django.contrib import admin
from notification import models

admin.site.register(models.Phone)
admin.site.register(models.Settings)
