from datetime import timedelta

from django.contrib.gis.db.models import PointField
from django.db import models
from django.utils import timezone
from route.models import Route


class Carrier(models.Model):
    name = models.CharField(max_length=30)
    icon = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.name


class Bus(models.Model):
    bus_number = models.CharField(verbose_name='Номер машины', max_length=30)
    bus_route = models.ForeignKey(Route, verbose_name='номер маршрута', null=True)
    bus_ritm_id = models.IntegerField(verbose_name='ritm id автобуса', null=True)
    carrier = models.ForeignKey(Carrier, null=True)

    def __str__(self):
        return self.bus_number

    @classmethod
    def get_online(cls):
        last_activity = timezone.now() - timedelta(minutes=5)
        return (
            BusInfo.objects
                .filter(last_activity__gte=last_activity)
                .select_related('bus', 'bus__bus_route')
        )


class BusInfo(models.Model):
    COURSE_CHOICE = (
        ('to', 'to'),
        ('from', 'from')
    )
    move = models.CharField(choices=COURSE_CHOICE, null=True, max_length=10)
    bus = models.ForeignKey(Bus, related_name='info')
    point = PointField(null=True)
    last_index = models.IntegerField(null=True)
    current_index = models.IntegerField(null=True)
    speed = models.FloatField()
    course = models.IntegerField()
    last_activity = models.DateTimeField()

    def __str__(self):
        return 'bus_id: {}'.format(
            self.bus_id
        )
