from bus import models
from django.contrib import admin

admin.site.register(models.Bus)
admin.site.register(models.BusInfo)
admin.site.register(models.Carrier)
