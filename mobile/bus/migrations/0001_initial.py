# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-04 06:56
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('route', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bus_number', models.CharField(max_length=30, verbose_name='Номер машины')),
                ('bus_ritm_id', models.IntegerField(null=True, verbose_name='ritm id автобуса')),
                ('bus_route', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='route.Route', verbose_name='номер маршрута')),
            ],
        ),
        migrations.CreateModel(
            name='BusInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('move', models.CharField(choices=[('to', 'to'), ('from', 'from')], max_length=10, null=True)),
                ('point', django.contrib.gis.db.models.fields.PointField(null=True, srid=4326)),
                ('last_index', models.IntegerField(null=True)),
                ('current_index', models.IntegerField(null=True)),
                ('speed', models.FloatField()),
                ('course', models.IntegerField()),
                ('bus', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='info', to='bus.Bus')),
            ],
        ),
    ]
