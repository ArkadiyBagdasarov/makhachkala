from bus.models import Bus, Carrier
from django.conf import settings
from django.core.management.base import BaseCommand
from geo_ritm.bus_info import GeoRitmApiCore
from route.models import Route


class Command(BaseCommand):
    def handle(self, *args, **options):
        # geo_ritm = GeoRitmApiCore()
        # buses = geo_ritm.get_all_bus()

        bus_list = [
            # '../dict_info/bus_125',
            # '../dict_info/bus_125a',
            # '../dict_info/bus_146',
            # '../dict_info/bus_reserve'
            '../dict_info/bus_2',
        ]
        for buses_number in bus_list:
            with open('{}/{}'.format(settings.BASE_DIR, buses_number)) as f:
                data = f.read().split('\n')

            for bus_number in data:
                # try:
                #     bus_id = (item for item in buses if item["name"] == bus_number).__next__()
                # except StopIteration:
                #     bus_id = {}
                route, _ = Route.objects.get_or_create(
                    name=buses_number.split('/')[-1][4:]
                )
                carrier = Carrier.objects.get(name='sts')
                Bus.objects.get_or_create(
                    bus_number=bus_number,
                    bus_route=route,
                    carrier=carrier
                    # bus_ritm_id=bus_id.get('id')
                )
