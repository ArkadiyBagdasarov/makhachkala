from django.db import models


class WialonAccessToken(models.Model):
    token = models.CharField(max_length=200)
