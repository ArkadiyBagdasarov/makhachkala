from rest_framework import status
from rest_framework.response import Response


def resp(body='', err=None, success=False, status_code=None):
    if not status_code:
        status_code = status.HTTP_200_OK if success else status.HTTP_400_BAD_REQUEST

    if not isinstance(err, list):
        err = [err]

    return Response({
        'success': success,
        'errors': err,
        'body': body
    }, status=status_code)
