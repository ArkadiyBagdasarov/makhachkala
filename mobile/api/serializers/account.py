from account.models import Account
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


def unique_phone(value):
    account_count = Account.objects.filter(
        phone=value
    ).count()

    if account_count != 0:
        raise ValidationError('Такой телефон уже зарегистрирован')


def unique_vk_token(value):
    account_count = Account.objects.filter(
        vk_token=value
    ).count()

    if account_count != 0:
        raise ValidationError('Такой пользователь уже зарегистрирован')


class RegisterSerializer(serializers.Serializer):
    phone = serializers.CharField(validators=[unique_phone])
    name = serializers.CharField(max_length=50)

    def validate(self, data):
        phone = data['phone']
        import re
        if not re.match(r'^7\d{10}$', phone):
            raise serializers.ValidationError({
                'phone': 'Укажите корректный телефон'})
        return data

    def create(self, validated_data):
        return Account.objects.create_account_user(
            name=validated_data['name'],
            phone=validated_data['phone']
        )


class RegisterVkSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)
    vk_token = serializers.CharField(max_length=100, validators=[unique_vk_token])
    vk_id = serializers.CharField(max_length=40)
    photo = serializers.CharField(required=False)

    def create(self, validated_data):
        user = Account.objects.create_account_vk_user(
            name=validated_data['name'],
            vk_token=validated_data['vk_token'],
            photo=validated_data.get('photo'),
            vk_id=validated_data['vk_id']
        )
        account = user.account
        account.generate_new_uuid()
        account.confirmed = True
        account.conf_code_resend_count = 0
        account.save(update_fields=['confirmed', 'conf_code_resend_count'])

        return user


class LoginSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=128)
    password = serializers.CharField(max_length=36)


class LoginVkSerializer(serializers.Serializer):
    vk_token = serializers.CharField(max_length=100)

    def validate(self, attrs):
        try:
            account = Account.objects.get(vk_token=attrs['vk_token'])
        except Account.DoesNotExist:
            raise serializers.ValidationError({
                'vk_token': 'Нет такого пользователя'})
        else:
            attrs['account'] = account
        return attrs


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('phone', 'name', 'photo', 'vk_id')


class CheckSmsCodeSerializer(serializers.Serializer):
    phone = serializers.CharField()
    sms_code = serializers.CharField()

    def validate(self, attrs):
        super(CheckSmsCodeSerializer, self).validate(attrs)
        phone = attrs['phone']

        import re
        if not re.match(r'^7\d{10}$', phone):
            raise serializers.ValidationError({
                'phone': 'Укажите корректный телефон'})
        try:
            account = Account.objects.get(phone=phone)
        except Account.DoesNotExist:
            raise ValidationError({
                'phone': 'Нет такого пользователя'})
        else:
            attrs['account'] = account

        if account.confirmation_code != attrs['sms_code']:
            raise ValidationError({
                'sms_code': 'Неверный код'})
        return attrs

    def create(self, validated_data):
        account = validated_data['account']
        account.generate_new_uuid()
        account.confirmed = True
        account.conf_code_resend_count = 0
        account.save(update_fields=['confirmed', 'conf_code_resend_count'])

        return account


class ResendSmsCodeSerializer(serializers.Serializer):
    phone = serializers.CharField()

    def validate(self, attrs):
        phone = attrs['phone']
        try:
            account = Account.objects.get(phone=phone)
        except Account.DoesNotExist:
            raise ValidationError({
                'phone': 'Нет такого пользователя'})
        else:
            attrs['account'] = account

        return attrs