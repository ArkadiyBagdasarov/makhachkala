from complaint.models import Complaint, Reason
from rest_framework import serializers


class ReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reason
        fields = ('smoke', 'deviation', 'speaking', 'stop_in_wrong_place')


class ComplaintSerializer(serializers.ModelSerializer):
    reason = ReasonSerializer()
    time_complaint = serializers.DateTimeField(required=False, format='%d-%m-%Y %H:%M:%S')

    class Meta:
        model = Complaint
        fields = ('route', 'bus', 'text', 'reason', 'time_complaint')

    def create(self, validated_data):
        user = self.context['user']
        validated_data['user'] = user
        complaint = Complaint.objects.create(**validated_data)
        Reason.objects.create(complaint=complaint, **validated_data['reason'])
        return complaint
