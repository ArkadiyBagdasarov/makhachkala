from rest_framework import serializers


class NearestStopBusSerializer(serializers.Serializer):
    point_start = serializers.ListField(
        child=serializers.FloatField()
    )
    point_stop = serializers.ListField(
        child=serializers.FloatField()
    )
