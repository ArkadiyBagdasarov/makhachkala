from rest_framework import serializers
from route.models import Route, RoutePath


class RoutePathSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoutePath
        fields = ('id', 'points_to', 'points_from', 'stop_to', 'stop_from')


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ('id', 'name', 'path', 'map')


class RoutePathInfoSerializer(RouteSerializer):
    route = RoutePathSerializer(source='path')

    class Meta:
        model = Route
        fields = ('id', 'name', 'route')
