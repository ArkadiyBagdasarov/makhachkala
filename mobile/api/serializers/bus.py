from api.serializers.route import RouteSerializer
from bus.models import BusInfo, Bus, Carrier
from rest_framework import serializers


class BusSerializer(serializers.ModelSerializer):
    bus_route = RouteSerializer()

    class Meta:
        model = Bus
        fields = ('id', 'bus_number', 'bus_route', 'bus_ritm_id', 'carrier')


class BusInfoSerializer(serializers.ModelSerializer):
    bus = BusSerializer()

    class Meta:
        model = BusInfo
        fields = (
            'bus', 'current_index',
            'speed', 'course', 'move'
        )


class CarrierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Carrier
        fields = '__all__'
