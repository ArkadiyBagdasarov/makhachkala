from api.serializers import base
from api.utils import resp
from nearest.utils import NearestCore
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView


class SetingsView(APIView):
    def get(self, request):
        # from route.management.commands.add_bus_in_bus_stop import Command
        # c = Command()
        # c.handle()
        # from periodic_tasks.tasks import get_bus_in_zones
        # get_bus_in_zones()
        return Response()


class NearestStopBusView(GenericAPIView):
    serializer_class = base.NearestStopBusSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        nearest = NearestCore(**serializer.validated_data)
        bus_stops = nearest.get_neirest()
        return resp(body=bus_stops, success=True)
