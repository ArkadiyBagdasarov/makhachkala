from datetime import timedelta

from account.tasks import send_sms_confirmation_code
from api.mixin import AuthenticatedMixin, NotAuthenticatedMixin
from api.serializers import account
from api.utils import resp
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView


class RegisterUserView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.RegisterSerializer

    def post(self, request, format=None):
        """
        Метод регистрации пользователя.
        ---
        """
        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        user = serializer.save()

        self._send_sms_code(user.id)

        return resp(success=True, status_code=status.HTTP_201_CREATED)

    def _send_sms_code(self, user_id):
        if settings.DEBUG:
            return

        send_sms_confirmation_code.delay(user_id)


class RegisterUserVkView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.RegisterVkSerializer

    def post(self, request, format=None):
        """
        Метод регистрации пользователя через VK.COM
        ---
        """
        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        user = serializer.save()
        login(request, user)

        request.session.set_expiry(60 * 60 * 24 * 7 * 12)

        return resp(success=True, status_code=status.HTTP_201_CREATED)


class LoginVkView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.LoginVkSerializer

    def post(self, request, format=None):
        """
        Метод авторизации пользователя с токеном VK.COM
        ---
        """

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        account = serializer.validated_data['account']

        user = authenticate(
            username=account.vk_token,
            password=account.token
        )

        if not user:
            return resp(err={'fields': ['Неверный логин или пароль']}, success=False)

        if not user.is_active:
            return resp(err={'fields': ['Пользователь неактивен']}, success=False)

        login(request, user)

        request.session.set_expiry(60 * 60 * 24 * 7 * 12)

        return resp(success=True)


class LoginView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.LoginSerializer

    def post(self, request, format=None):
        """
        Метод авторизации пользователя.
        ---
        """

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        user = authenticate(
            username=serializer.validated_data['login'],
            password=serializer.validated_data['password']
        )

        if not user:
            return resp(err={'fields': ['Неверный логин или пароль']}, success=False)

        if not user.is_active:
            return resp(err={'fields': ['Пользователь неактивен']}, success=False)

        login(request, user)

        request.session.set_expiry(60 * 60 * 24 * 7 * 12)

        return resp(success=True)


class LogoutView(AuthenticatedMixin, APIView):
    def post(self, request, format=None):
        """
        Метод разавторизации пользователя.
        """

        logout(request)
        return resp(success=True)


class AccountView(AuthenticatedMixin, APIView):
    account_serializer = account.AccountSerializer

    def get(self, request, format=None):
        """
        Метод получения информации об аккаунте
        """
        account = request.user.account
        if not account:
            return resp(body={
                'user_id': request.user.id,
                'user_email': request.user.email
            }, success=True)
        else:
            serializer = self.account_serializer(account)

        return resp(body=serializer.data, success=True)


class CheckSmsCodeView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.CheckSmsCodeSerializer

    def post(self, request, format=None):
        """
        Метод проверки регистрационнго смс-кода.
        ---
        """

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        account = serializer.save()
        self._authenticate_user(account.user)

        request.session.set_expiry(60 * 60 * 24 * 7 * 12)

        return resp(body={'password': account.token}, success=True)

    def _authenticate_user(self, user):
        login(self.request, user)


class ResendSmsCodeView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = account.ResendSmsCodeSerializer

    def post(self, request, format=None):
        """
        Метод повторной отправки регистрационного смс-кода
        ---
        """
        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        account = serializer.validated_data['account']

        # if account.conf_code_resend_count == 3:
        #     return resp(err=['Превышено максимальное число запросов'], success=False)

        if account.conf_code_last_sent_time is not None:
            t = account.conf_code_last_sent_time + timedelta(minutes=1)
            now = timezone.now()
            if t > now:
                return resp(err={'error': 'Превышена частота запросов',
                                 'sec_remain': (t - now).seconds}, success=False)

        # account.conf_code_resend_count += 1
        # account.save()

        self._send_sms_code(account.user_id)

        return resp(success=True)

    def _send_sms_code(self, user_id):
        if settings.DEBUG:
            return

        send_sms_confirmation_code.delay(user_id)