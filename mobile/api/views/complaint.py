from api.mixin import AuthenticatedMixin
from api.serializers import complaint
from api.utils import resp
from rest_framework.generics import GenericAPIView


class ComplaintView(AuthenticatedMixin, GenericAPIView):
    serializer_class = complaint.ComplaintSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={'user': request.user}
        )

        if not serializer.is_valid():
            return resp(err=serializer.errors, success=False)

        serializer.save()

        return resp(body=serializer.data, success=True)
