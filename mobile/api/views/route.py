from api.serializers import route
from api.utils import resp
from route.models import RoutePath, Route
from rest_framework.views import APIView


class RoutePathView(APIView):
    """
    Метод получения маршрутов.
    """

    serializer_class = route.RoutePathInfoSerializer

    def get(self, request):
        routes_path = Route.objects.select_related('path')
        serializer = (
            self.serializer_class(
                routes_path,
                many=True
            )
        )
        return resp(body=serializer.data, success=True)
