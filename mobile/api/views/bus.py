from api.mixin import NotAuthenticatedMixin
from api.serializers import bus
from api.utils import resp
from bus.models import Bus, Carrier
from rest_framework.generics import GenericAPIView


class BusOnlineView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = bus.BusInfoSerializer

    def post(self, request):
        """
        Метод получения реальных online автобусов.
        """
        data = Bus.get_online()
        serializer = self.serializer_class(data, many=True)
        return resp(body=serializer.data, success=True)


class СarrierView(NotAuthenticatedMixin, GenericAPIView):
    serializer_class = bus.CarrierSerializer

    def get(self, request):
        """
        Метод получения перевозчиков.
        """
        carriers = Carrier.objects.all()
        serializer = self.serializer_class(carriers, many=True)

        return resp(body=serializer.data, success=True)
