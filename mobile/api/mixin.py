from django.conf import settings

from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import (
    TokenAuthentication, SessionAuthentication
)


class CustomSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        if not settings.API_CHECK_CSRF:
            return

        return super().enforce_csrf(request)


class AuthenticatedMixin:
    authentication_classes = (CustomSessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)


class NotAuthenticatedMixin:
    authentication_classes = (CustomSessionAuthentication, TokenAuthentication)
