from api.views import base, bus, route, complaint, account
from django.conf.urls import url

urlpatterns = [

    url(r'^settings/$', base.SetingsView.as_view()),

    url(r'^route/path/$', route.RoutePathView.as_view()),
    url(r'^bus/online/$', bus.BusOnlineView.as_view()),
    url(r'^bus/stop/nearest/$', base.NearestStopBusView.as_view()),
    url(r'^complaint/$', complaint.ComplaintView.as_view()),
    url(r'^accounts/register/$', account.RegisterUserView.as_view()),
    url(r'^accounts/register_vk/$', account.RegisterUserVkView.as_view()),
    url(r'^accounts/login/$', account.LoginView.as_view()),
    url(r'^accounts/login_vk/$', account.LoginVkView.as_view()),

    url(r'^carrier/$', bus.СarrierView.as_view()),

    url(r'^accounts/view/$', account.AccountView.as_view()),
    url(r'^accounts/logout/$', account.LogoutView.as_view()),
    url(r'^accounts/checksms/$', account.CheckSmsCodeView.as_view()),
    url(r'^accounts/resendsms/$', account.ResendSmsCodeView.as_view()),
]
