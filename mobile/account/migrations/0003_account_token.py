# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-15 06:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170913_1659'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='token',
            field=models.CharField(max_length=36, null=True, verbose_name='токен доступа'),
        ),
    ]
