import random

from celery import shared_task
from django.contrib.auth.models import User
from django.utils import timezone
from notification.utils import send_sms


@shared_task(name='send_sms_confirmation_code')
def send_sms_confirmation_code(user_id):

    user = User.objects.get(id=user_id)
    account = user.account

    code = str(random.randint(10000, 99999))

    send_time = timezone.now()

    send_sms(account.phone, code)
    account.confirmed = False
    account.confirmation_code = code
    account.conf_code_last_sent_time = send_time
    account.save()
