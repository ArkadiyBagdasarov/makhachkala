import uuid

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.db import models, transaction


class AccountManager(models.Manager):
    @classmethod
    def create_account_user(cls, phone, name, **kwargs):
        with transaction.atomic():
            user = User.objects.create_user(
                username=phone
            )
            Account.objects.create(
                phone=phone,
                name=name,
                user=user,
                **kwargs
            )

        return user

    @classmethod
    def create_account_vk_user(cls, vk_token, name, **kwargs):
        with transaction.atomic():
            user = User.objects.create_user(
                username=vk_token
            )
            Account.objects.create(
                vk_token=vk_token,
                name=name,
                user=user,
                **kwargs
            )

        return user


class Account(models.Model):
    objects = AccountManager()

    user = models.OneToOneField(User, null=True)
    phone = models.CharField('Номер телефона', max_length=11, unique=True, null=True)
    name = models.CharField('имя', max_length=50)
    token = models.CharField('токен доступа', max_length=36, null=True)
    vk_token = models.CharField('токен vk.vom', max_length=100, unique=True, null=True, blank=True)
    vk_id = models.CharField('id user vk.com', max_length=50, null=True, blank=True)
    photo = models.CharField('аватар пользователя', max_length=300, null=True, blank=True)

    confirmation_code = models.CharField('Код подтверждения', max_length=10,
                                         null=True, blank=True)
    conf_code_last_sent_time = models.DateTimeField('Время последней отправки',
                                                    null=True, blank=True)
    conf_code_resend_count = models.IntegerField(
        verbose_name='Сколько раз отправляли по новой',
        default=0)
    last_activity_time = models.DateTimeField(null=True, blank=True)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.phone or self.name

    def generate_new_uuid(self):
        self.token = uuid.uuid4().hex[:10]
        self.user.set_password(self.token)
        self.user.save()
        self.save(update_fields=['token'])
